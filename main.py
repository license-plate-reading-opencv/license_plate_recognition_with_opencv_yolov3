import os

import cv2
import numpy as np
import matplotlib.pyplot as plt
import easyocr

import util



# Get the names of the output layers
def getOutputsNames(net):
    # Get the names of all the layers in the network
    layersNames = net.getLayerNames()
    print(layersNames)
    # Get the names of the output layers, i.e. the layers with unconnected outputs
    #return [layersNames[i[1] - 1] for i in net.getUnconnectedOutLayers()]
    return 0
# Remove the bounding boxes with low confidence using non-maxima suppression
def postprocess(frame, outs):
    frameHeight = frame.shape[0]
    frameWidth = frame.shape[1]

    # Scan through all the bounding boxes output from the network and keep only the
    # ones with high confidence scores. Assign the box's class label as the class with the highest score.
    classIds = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > confThreshold:
                center_x = int(detection[0] * frameWidth)
                center_y = int(detection[1] * frameHeight)
                width = int(detection[2] * frameWidth)
                height = int(detection[3] * frameHeight)
                left = int(center_x - width / 2)
                top = int(center_y - height / 2)
                classIds.append(classId)
                confidences.append(float(confidence))
                boxes.append([left, top, width, height])

confThreshold = 0.5  #Confidence threshold
nmsThreshold = 0.4   #Non-maximum suppression threshold
inpWidth = 416       #Width of network's input image
inpHeight = 416      #Height of network's input image

net = cv2.dnn.readNetFromDarknet("./model/cfg/darknet_yolov3.cfg","./model/weights/model.weights")
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
with open("./model/class/classes.names","r") as f:
    classes = [line.strip() for line in f.readlines()]


#initialize video
cap = cv2.VideoCapture(0)
cv2.namedWindow("frame",cv2.WINDOW_NORMAL)
while True:
    ret,frame = cap.read()
    if not ret:
        print("Error while capturing frames")
        break
    H, W, _ = frame.shape
    blob = cv2.dnn.blobFromImage(frame, 1/255, (inpWidth, inpHeight), [0,0,0], 1, crop=False)
    # Sets the input to the network
    net.setInput(blob)
    # Runs the forward pass to get output of the output layers
    outs = util.get_outputs(net)
      # bboxes, class_ids, confidences
    bboxes = []
    class_ids = []
    scores = []
    for detection in outs:
        # [x1, x2, x3, x4, x5, x6, ..., x85]
        bbox = detection[:4]

        xc, yc, w, h = bbox
        bbox = [int(xc * W), int(yc * H), int(w * W), int(h * H)]

        bbox_confidence = detection[4]

        class_id = np.argmax(detection[5:])
        score = np.amax(detection[5:])

        bboxes.append(bbox)
        class_ids.append(class_id)
        scores.append(score)

    # apply nms
    bboxes, class_ids, scores = util.NMS(bboxes, class_ids, scores)

    # plot
    reader = easyocr.Reader(['en'])
    for bbox_, bbox in enumerate(bboxes):
        xc, yc, w, h = bbox

        # cv2.putText(img,
        #             class_names[class_ids[bbox_]],
        #             (int(xc - (w / 2)), int(yc + (h / 2) - 20)),
        #             cv2.FONT_HERSHEY_SIMPLEX,
        #             7,
        #             (0, 255, 0),
        #             15)
        license_plate = frame[int(yc - (h / 2)):int(yc + (h / 2)),int(xc - (w / 2)):int(xc + (w / 2)),:].copy()
        '''frame = cv2.rectangle(frame,
                            (int(xc - (w / 2)), int(yc - (h / 2))),
                            (int(xc + (w / 2)), int(yc + (h / 2))),
                            (0, 255, 0),
                            2)'''
        output = reader.readtext(license_plate)
        print(output)
    #postprocess(frame, outs)
    if cv2.waitKey(1) == ord('q'):
        break
    cv2.imshow("frame",frame)
    
cap.release()
cv2.destroyAllWindows()
